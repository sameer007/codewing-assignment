<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $loggedInUserData = $this->session()->get('loggedInUserData');
        return isset($loggedInUserData) && !empty($loggedInUserData) ? true : false ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        
        return [
            'json_file' => 'required|file|mimes:json',
        ];
    }

    /**
     * Customize validation error messages
     */
    public function messages()
    {
        return [
            'json_file.required' => 'The json_file field is required.',
            'json_file.file' => 'The json_file field must be a file.',
            'json_file.max' => 'The json_file field must not exceed 2048 characters.',
        ];
    }
}
