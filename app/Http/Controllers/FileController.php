<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\FileUploadRequest;
use App\Services\ExcelService;
use Illuminate\Http\Request;

class FileController extends Controller
{
    private $excelService;

    public function __construct(ExcelService $excelService)
    {
        $this->excelService = $excelService;
    }

    /**
     * form validation and excel to json conversion
     */
    public function getExcelFromJson(FileUploadRequest $request)
    {
        $jsonFile = $request->file('json_file');
        return $this->excelService->convertJsonFileToExcel($jsonFile);
    }
}
