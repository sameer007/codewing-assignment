<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    /** 
     * display dashboard page with logged in user data
     */
    public function showDashboard(Request $request)
    {
        $loggedinUserData = Auth::user();
        if(isset($loggedinUserData) && !empty($loggedinUserData)) { 
            Session::put('loggedInUserData', $loggedinUserData);
            
            return view('dashboard',$loggedinUserData);
        }else{
            return back()->with([
                'error' => 'Something went wrong while authenticating user',
            ]);
        }
        
    }
    /**
     * logout user and invalidate session
     */
    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/')->with([
            'success' => 'Logged out successfully',
        ]);
    }
}
