<?php

namespace App\Services;

use App\Exports\ExcelExport;
use Exception;
use Maatwebsite\Excel\Facades\Excel;

class ExcelService  
{   
    /**
     * Json to excel conversion
     */
    public function convertJsonFileToExcel($uploadedJsonFile)
    {
        try {
            //get original file name
            $originalFileNameWithExtention = $uploadedJsonFile->getClientOriginalName();
            $originalFileName = substr($originalFileNameWithExtention, 0, strrpos($originalFileNameWithExtention, '.')); 
            
            //json to excel
            $jsonDataArray = json_decode(file_get_contents($uploadedJsonFile), true);
            $export = new ExcelExport($jsonDataArray);
            return Excel::download($export, $originalFileName.'.xlsx');
        } catch (Exception $e) {
            throw $e;
        }
        
        
    }
}