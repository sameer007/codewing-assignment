<?php

namespace App\Exports;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExcelExport implements FromArray, WithStyles, WithHeadings,ShouldQueue
{
    use Exportable,Queueable;
    
    protected $invoices;
    protected $headings;

    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
        $this->headings = array_keys($invoices[0]);

    }

    public function array(): array
    {
        return $this->invoices;
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
    public function headings(): array
    {
        return [
            $this->headings
        ];
    }
}
