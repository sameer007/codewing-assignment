<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // if (Auth::check()) {
    //     return redirect('home/dashboard');
        
    // }else{
        return view('index');

    //}
});
Route::group(['prefix' => 'auth'],function () {

    Route::get('/callback', [LoginController::class , 'callback']);
    Route::get('/login', [LoginController::class , 'redirectToGithub'])->name('login');
    Route::get('/logout', [DashboardController::class,'logout'])->name('logout');

    
});

Route::group(['prefix' => 'home'],function () {
    Route::get('/dashboard', [DashboardController::class,'showDashboard'])->name('showDashboard');
    Route::post('/jsonToExcel', [FileController::class,'getExcelFromJson']);
});

Route::fallback(function () {
    return redirect('/')->with([
        'error' => 'Invalid route detected'
    ]);
});