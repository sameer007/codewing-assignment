

const url = 'https://pkgstore.datahub.io/core/nasdaq-listings/nasdaq-listed_json/data/a5bc7580d6176d60ac0b2142ca8d7df6/nasdaq-listed_json.json';

//GET request to the URL
fetch(url)
    .then(response => {
        // Check if the response status is OK (status code 200)
        if (!response.ok) {
            throw new Error(`Network response was not ok: ${response.status}`);
        }
        // Parse the JSON response
        return response.json();
    })
    .then(jsonData => {
        // Use the JSON data here
        //console.log('jsonData>>',jsonData);

        const parsedJsonData = new Set();

        // Iterate through the dataset and extract symbols
        jsonData.forEach(item => {
            // Assuming the symbol is stored in a property named "Symbol"
            //const jsonItem = item; // Replace "Symbol" with the actual property name
            if (symbol) {
                parsedJsonData.add(item);
            }
        });

        // Convert the Set to an array if needed
        const distinctCompanyData = Array.from(parsedJsonData);

        

        const selectElement = $("#symbol");

        // Iterate through the distinctSymbols array and add each symbol as an option
        distinctCompanyData.forEach(item => {
            // Create a new <option> element
            const optionElement = $("<option>");
            //console.log('distinctCompanyData Item>>', item);
            // Set the value and text of the <option>
            optionElement.val(item["Symbol"]);
            optionElement.text(item["Company Name"] + '  (' + item["Symbol"] + ')');

            // Append the <option> to the <select>
            selectElement.append(optionElement);
        });

    })
    .catch(error => {
        // Handle errors
        console.error('There was a problem with the fetch operation:', error);
    });
