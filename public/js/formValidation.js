$(document).ready(function() {
    // Get the current date in YYYY-MM-DD format
    var currentDate = new Date().toISOString().split('T')[0];
    var dobDate = $('#dob');
    var startDate = $('#startDate');
    
    // Set the 'max' attribute of the "End Date" input to the current date
    dobDate.attr('max', currentDate);

});