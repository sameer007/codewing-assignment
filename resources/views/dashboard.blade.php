@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <h3><i class="fa fa-angle-right"></i>Dashboard</h3>
            @include('layouts.notify')
            <div class="row mt">
                <div class="col-lg-12">
                    @php
                    
                    @endphp
                    <p>JSON to excel converter by codewing
                    </p>
                    <div class="form-panel">
                        <h4 class="mb"><i class="fa fa-angle-right"></i> <span id="title">Convert JSON to EXCEL</span></h4>
                        <form id="importArtistForm" action="/home/jsonToExcel" enctype="multipart/form-data" METHOD="POST">
                            @csrf()
                            <div class="box-body"></div>

                            <div class="form-group col-md-6">
                                <label>Import JSON file</label>
                                <small class="req"> *</small>
                                <div class="col-md-4">
                                    <input id="jsonFile" type="file" name="json_file" accept=".json" class="default" />
                                </div>
                                <span class="text-danger"></span>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit" value="import-json">Convert JSON to EXCEL</button></div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    
    
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    @endsection