<aside>
    @php

    $currentPage = substr(strrchr(url()->current(),"/"),1);

    $loggedInUserData = session()->get('loggedInUserData');

    if(isset($loggedInUserData) && !empty($loggedInUserData)){

        if(isset($loggedInUserData->image) && !empty($loggedInUserData->image)){
            $profileUrl = asset('storage/images/profile/'.$loggedInUserData->image);

        }else{
            $profileUrl = url('https://cloud.xm-cdn.com/static/xm/common/logos/XMLogo-2021_homepage.svg');


        }
        
    }else{
        $profileUrl = url('https://cloud.xm-cdn.com/static/xm/common/logos/XMLogo-2021_homepage.svg');

    }
    //dd($loggedInUserData);
    @endphp

    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <p class="centered"><a href="profile.html"><img src="{{$profileUrl}}" class="img-square" width="80"></a></p>
            <h5 class="centered" id="loggedInUser"></h5>
            <li class="mt">
                <a class="{{ ($currentPage == 'home/dashboard') ? 'active' : ''  }}" href="{{url('home/dashboard')}}">
                    <i class="fa fa-table"></i>
                    <span>Dashboard</span>
                </a>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>