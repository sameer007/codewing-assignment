@php

$currentPage = substr(strrchr(url()->current(),"/"),1);

$loggedInUserData = session()->get('loggedInUserData');

if(isset($loggedInUserData) && !empty($loggedInUserData)){
    
    if(isset($loggedInUserData->image) && !empty($loggedInUserData->image)){
        $profileUrl = asset('storage/images/profile/'.$loggedInUserData->image);

    }else{
        $profileUrl = url('img/ui-sam.jpg');

    }
}else{
    $profileUrl = url('img/ui-sam.jpg');
}
@endphp
<section id="container">
    <header class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="index.html" class="logo"><b>CODEWING <span> {{$loggedInUserData->name}} </span></b></a>
        <!--logo end-->
        <div class="top-menu">
            <ul class="nav pull-right top-menu">
                <li><a class="logout" id="logout" onclick="return confirm('Are you sure you want to logout?')" href="{{ url('auth/logout') }}">Logout</a></li>
            </ul>
        </div>
    </header>