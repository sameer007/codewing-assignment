@extends('layouts.app')

@section('content')
<section id="container">


    <section class="panel">
        <div class="panel-body ">
            <div class="view-mail">
                <p>Dear HR Manager,</p>
                <br/>
                <p>We hope this email finds you well.This email is to inform you that we have retreived historical data of your company's share prices.We believe this data is important for your firm</p>
                <p>This data dates back from {{$startDate}} to {{$endDate}}</p>
                <p>If you want to enquire more about this data you can send us an email.</p>
                
                <br/>
                <p>Best regards,</p>
                <p>Sameer Acharya</p>

            </div>
        </div>
    </section>
</section>


@endsection