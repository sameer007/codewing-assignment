@extends('layouts.app')

@section('content')
<div id="login-page">
  <div class="container">
    
    <form class="form-login" id="myForm" action="{{route('login')}}"  method="get">
      <h2 class="form-login-heading">Login now</h2>
      @include('layouts.notify')
      <div class="login-wrap">
        
        <button class="btn btn-theme btn-block" href="{{url('./auth/login')}}" type="submit"><i class="fa fa-lock"></i> LOGIN WITH GITHUB</button>
        <hr>
      </div>
      <!-- modal -->
    </form>
  </div>
</div>
@endsection
@section('customJs')
<!-- js placed at the end of the document so the pages load faster -->
<script src="{{url('lib/jquery/jquery.min.js')}}"></script>
<script src="{{url('lib/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('lib/jquery-ui-1.9.2.custom.min.js')}}"></script>
<script src="{{url('lib/jquery.ui.touch-punch.min.js')}}"></script>
<script class="include" type="text/javascript" src="{{url('lib/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{url('lib/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('lib/jquery.nicescroll.js')}}" type="text/javascript"></script>
<!--common script for all pages-->
<script src="{{url('lib/common-scripts.js')}}"></script>
<!--script for this page-->
<script type="text/javascript" src="{{url('lib/jquery.backstretch.min.js')}}"></script>
<script>
  $.backstretch("{{url('img/login-bg.jpg')}}", {
    speed: 500
  });
</script>

@endsection